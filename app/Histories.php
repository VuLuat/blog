<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Histories extends Model
{
    protected $table = 'histories';
    protected $fillable = [
        'kanji',
        'hiragana',
    ];

    public function users(){
        return $this->belongsToMany(User::class,'history_user','user_id','history_id');
    }
}
