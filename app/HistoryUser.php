<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryUser extends Model
{
    protected $table = 'history_user';
    protected $fillable = [
        'user_id',
        'history_id',
    ];
}
