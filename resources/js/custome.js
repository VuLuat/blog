
import Kuroshiro from "kuroshiro";
import KuromojiAnalyzer from "kuroshiro-analyzer-kuromoji";

// Instantiate:
$(document).ready(function () {

    if ($('#btnConvert').length > 0) {
        document.getElementById("btnConvert").addEventListener('click', function () {
            convert();
        });
    }
});
async function convert() {
    const kuroshiro = new Kuroshiro();
    await kuroshiro.init(new KuromojiAnalyzer(
        {
            dictPath: '/node_modules/kuromoji/dict',
        }
    ));
    var kanjiValue = $('#kanji').val();
    const result = await kuroshiro.convert(kanjiValue, { to: "hiragana" });

    $('#hiragana').val(result);

}

