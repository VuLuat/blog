<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
    <br>
    <div class="container">
        <div style="text-align: right" class="row">
            <div class="">
                <a href="{{ url('login/facebook') }}" class="btn btn-primary @if(auth()->check())d-none @endif ">Login with Facebook</a>
            </div>
            @auth
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{Auth::user()->name}}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <button type="submit" class="dropdown-item">Check history</button>
                        <form action="{{route('logout',Auth::user()->id)}}" method="post">
                            @csrf
                            <button type="submit" class="dropdown-item">Log out</button>
                        </form>
                    </div>
                </div>
            @endauth
        </div>
        <br>

        <div class=" @if(auth()->check()) d-none @endif row">
            <div class="d-inline-block col-md-6">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Add your Kanji text:</label>
                        <textarea class="form-control" id="kanji" rows="4" name="kanji"></textarea>
                        <br>
                    </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        <label for="exampleFormControlTextarea1">Hiragana text:</label>
                        <textarea class="form-control" id="hiragana" rows="4" name="hiragana"></textarea>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" id="btnConvert" class="btn btn-primary">convert</button>
            </div>
        </div>

    @auth()
            <form action="{{route('history.store',Auth::user()->id)}}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Add your Kanji text:</label>
                        <textarea class="form-control" id="kanji" rows="4" name="kanji"></textarea>
                        <br>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Hiragana text:</label>
                        <textarea class="form-control" id="hiragana" rows="4" name="hiragana"></textarea>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" id="btnConvert" class="btn btn-primary">convert</button>
                </div>
            </div>
            </form>
        @endauth
    </div>
<script src="/js/app.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>


